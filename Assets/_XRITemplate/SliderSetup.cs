using System.Collections;
using System.Collections.Generic;
using Unity.XRContent.Interaction;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

public class SliderSetup : MonoBehaviour
{
    [SerializeField]
    [Tooltip("Stores the Slider used to set the move speed of continuous movement.")]
    XRSlider m_MoveSpeedSlider;

    [SerializeField]
    [Tooltip("Stores the behavior that will be used to configure locomotion control schemes and configuration preferences.")]
    //XRRigLocomotionManager m_Manager;

    const float k_MinMoveSpeed = 0.5f;
    const float k_MaxMoveSpeed = 5.0f;
    
    private void InitializeControl()
    {
        //m_MoveSpeedSlider.Value = Mathf.InverseLerp(k_MinMoveSpeed, k_MaxMoveSpeed, m_Manager.SmoothMoveProvider.moveSpeed);
    }

    private void ConnectControlEvents()
    {
        m_MoveSpeedSlider.OnValueChange.AddListener(SetMoveSpeed);
    }

    private void DisconnectControlEvents()
    {
        m_MoveSpeedSlider.OnValueChange.RemoveListener(SetMoveSpeed);
    }

    void SetMoveSpeed(float sliderValue)
    {
        Debug.Log("slider value: "+sliderValue);
        // m_Manager.SmoothMoveProvider.moveSpeed = Mathf.Lerp(k_MinMoveSpeed, k_MaxMoveSpeed, sliderValue);
        // m_MoveSpeedLabel.text = $"{m_Manager.SmoothMoveProvider.moveSpeed.ToString(k_SpeedFormat)}{k_SpeedLabel}";
    }

    protected void OnEnable()
    {
        ConnectControlEvents();
        //InitializeControls();
    }

    protected void OnDisable()
        {
            DisconnectControlEvents();
        }
}
