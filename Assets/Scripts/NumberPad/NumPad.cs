using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class NumPad : MonoBehaviour
{
    #region Fields
    [SerializeField]
    [Tooltip("Screen where show the password numbers")]
    private ScreenText screenPanel;

    [SerializeField]
    [Tooltip("Text where show the today password")]
    private TodayPassScreen todayPassScreen;

    /// <summary>
    /// Queue where keep the original pass code    
    /// </summary>
    private Queue<int> code;

    /// <summary>
    /// Queue where keep pass from the user    
    /// </summary>
    private Queue<int> buttonNumbers;

    /// <summary>
    /// Flag for activate / deactivate the press button functionality
    ///     for the user    
    /// </summary>
    public static bool CanPressButton {get; set;}
    #endregion
    
    #region Create Password
    /// <summary>
    /// Initialize elements    
    /// </summary>
    private void Start()
    {
        CanPressButton = false;  
        buttonNumbers = new Queue<int>();
        code = new Queue<int>();
        CreateCode();
    }

    /// <summary>
    /// Creates a password code    
    /// </summary>
    private void CreateCode()
    {
        int num;
        todayPassScreen.SetNewPassScreenColor();
        todayPassScreen.ChangeTodayPassText("");
        for(int i=0; i<4; i++)
        {
            num = Random.Range(0, 9);
            todayPassScreen.TextTodayPassText = num;
            code.Enqueue(num);
        } 
        CanPressButton = true;    
    }
    #endregion
    
    #region OnPressButton and OnIncorrectPassword Events
    /// <summary>
    /// Register the OnPressButton event
    /// Register the OnIncorrectPassword event
    /// </summary>
    private void OnEnable() {
        PadButtonControl.OnPressButton += ButtonPressed;
        ScreenText.OnIncorrectPassword += CreateCode;
    }

    /// <summary>
    /// Unregister the OnPressButton event
    /// Register the OnIncorrectPassword event
    /// </summary>
    private void OnDisable() {
        PadButtonControl.OnPressButton -= ButtonPressed;
        ScreenText.OnIncorrectPassword -= CreateCode;
    }

    /// <summary>
    /// Event triggered by button
    /// - register the button number on queue and
    ///     send the number to screen panel
    /// </summary>
    private void ButtonPressed(int num){
        CanPressButton = false;
        buttonNumbers.Enqueue(num);        
        screenPanel.WriteNum(num);

        if(buttonNumbers.Count == code.Count) { ComparePassword(); }
        else { CanPressButton = true; }        
    }
    #endregion

    #region Compare Passwords
    /// <summary>
    /// Compare if the 2 numbers' queues are equals and
    ///     send the result to the screen panel
    /// </summary>
    private void ComparePassword(){
        bool equals = true;
        while(equals && code.Count != 0)
        {
            int num1 = code.Dequeue();
            int num2 = buttonNumbers.Dequeue();
            if( num1 != num2 ) {equals = false;}
        }

        if(equals) {screenPanel.SetCorrectAdvice();}
        else 
        {
            screenPanel.SetIncorrectAdvice();
            code.Clear();
            buttonNumbers.Clear();
        }
    }
    #endregion

    
}
