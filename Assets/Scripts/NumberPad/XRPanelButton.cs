using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.XR.Interaction.Toolkit;
using TMPro;

/// <summary>
/// An interactable that can be pushed by a direct interactor's movement
/// </summary>
public class XRPanelButton : XRBaseInteractable
{
    #region Serialized Fields
    [SerializeField]
    [Tooltip("Panel Button Control")]
    PadButtonControl buttonControl;    
    #endregion
    
    #region Private Fields    
    /// <summary>
    /// Reference of specific hand XR Controller
    /// </summary>
    private XRBaseController m_Controller;
    #endregion    

    /// <summary>
    /// Actions when controller hover entered the button    
    /// </summary>
    protected override void OnHoverEntered(HoverEnterEventArgs args)
    {
        if(NumPad.CanPressButton)
        {
            base.OnHoverEntered(args);
            buttonControl.ChangePressButtonMaterial();        
            buttonControl.PlayClickButtonSound();
            SetHapticImpulse(args);
            buttonControl.SendNumberToNumberPad();
        }        
    }

    /// <summary>
    /// Actions when controller hover exited the button    
    /// </summary>
    protected override void OnHoverExited(HoverExitEventArgs args)
    {
        base.OnHoverExited(args);
        buttonControl.ChangeReleaseButtonMaterial();
    }

    /// <summary>
    /// Calls a controller haptic impulse
    /// </summary>
    private void SetHapticImpulse(HoverEnterEventArgs args)
    {
        var controllerInteractor = args.interactorObject as XRBaseControllerInteractor;
        m_Controller = controllerInteractor.xrController;
        m_Controller.SendHapticImpulse(1, 0.5f);
        Debug.Log(m_Controller);
    }
}
