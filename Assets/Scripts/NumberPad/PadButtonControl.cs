using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class PadButtonControl : MonoBehaviour
{
    #region Serialized Fields
    [SerializeField]
    [Tooltip("Number Button Label")]
    private TextMeshProUGUI numberLabel;

    [SerializeField]
    [Tooltip("Number initial value")]
    private string numberValue;    
    #endregion
    
    #region Private Fields
    /// <summary>
    /// Reference for change button materials
    /// </summary>
    private ChangeMaterial buttonMaterial;
    /// <summary>
    /// Reference for play click sound
    /// </summary>
    private PlayQuickSound buttonClickSound;
    #endregion
    
    public delegate void PressButtonAction(int num);
    public static event PressButtonAction OnPressButton;

    /// <summary>
    /// Initialize fields
    /// </summary>
    private void Start(){        
        buttonMaterial = GetComponent<ChangeMaterial>();
        buttonClickSound = GetComponent<PlayQuickSound>();
        numberLabel.text = numberValue;
    }

    /// <summary>
    /// Change default material to pressed material
    /// </summary>
    public void ChangePressButtonMaterial()
    {
        buttonMaterial.SetMatOther();
    }

    /// <summary>
    /// Change pressed material to default material
    /// </summary>
    public void ChangeReleaseButtonMaterial()
    {
        buttonMaterial.SetMatDefault();
    }

    /// <summary>
    /// Plays click button sound
    /// </summary>
    public void PlayClickButtonSound()
    {
        buttonClickSound.Play();
    }

    /// <summary>
    /// Send the specific number to number pad
    /// </summary>
    public void SendNumberToNumberPad()
    {
        OnPressButton(int.Parse(numberLabel.text));       
    }
}
