using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class TodayPassScreen : MonoBehaviour
{
    [SerializeField]
    [Tooltip("Text where show the today password")]
    private TextMeshProUGUI textTodayPass;

    [SerializeField]
    [Tooltip("Today Pass Screen")]
    private Image imageScreen;

    public int TextTodayPassText {
        set{textTodayPass.text += value;}
    }

    public void ChangeTodayPassText(string text)
    {
        textTodayPass.text = text;
    }

    public void SetNewPassScreenColor()
    {               
        StartCoroutine(ChangeColorAndWait());        
    }
    private IEnumerator ChangeColorAndWait()
    {
        Color32 originalColor = imageScreen.color; 
        imageScreen.color = Color.green;
        yield return new WaitForSeconds(0.2f);
        imageScreen.color = originalColor;
        yield return new WaitForSeconds(0.2f);
        imageScreen.color = Color.green;
        yield return new WaitForSeconds(0.2f);
        imageScreen.color = originalColor;
    }
}
