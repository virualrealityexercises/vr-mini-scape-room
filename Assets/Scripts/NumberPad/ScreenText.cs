using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ScreenText : MonoBehaviour
{
    #region Serialized Fields
    [SerializeField] 
    [Tooltip("Color for normal text and numbers")] 
    private Color32 textColor;

    [SerializeField]
    [Tooltip("Color for correct text and numbers")] 
    private Color32 correctColor;

    [SerializeField]
    [Tooltip("Color for incorrect text and numbers")]
    private Color32 incorrectColor;

    [SerializeField] 
    [Tooltip("Text for screen number")]
    private TextMeshProUGUI passText;

    [SerializeField] 
    [Tooltip("Prefab of kerkard to instantiate")] 
    private GameObject keyCardPrefab;

    [SerializeField] 
    [Tooltip("Spawn position for intantiate prefab")] 
    private Transform keyCardSpawnPosition;
    #endregion
    
    #region Private fields
    /// <summary>
    /// Default message    
    /// </summary>
    private string defaultMessage = "Enter password";

    /// <summary>
    /// Default text size    
    /// </summary>
    private float defaultTextSize = 66f;

    /// <summary>
    /// Default number text size    
    /// </summary>
    private float textNumberSize = 140.6f;

    /// <summary>
    /// Flag for text number configuration    
    /// </summary>
    private bool isNumTextConfigured;

    /// <summary>
    /// Script for play correct and incorrect sound clips   
    /// </summary>
    private PlaySoundsFromList soundsList;  
    #endregion
    
    public delegate void IncorrectPassAction();
    public static event IncorrectPassAction OnIncorrectPassword;


    #region Initialization
    private void Awake() {
        soundsList = GetComponent<PlaySoundsFromList>();
    }
    
    private void Start() {    
        Initialize();
    }

    /// <summary>
    /// Initial configuration for default text and warn that the 
    ///     text number has not been configured
    /// </summary>
    private void Initialize() 
    {
        ConfigText(defaultMessage, defaultTextSize, FontStyles.Bold);
        passText.color = textColor;
        isNumTextConfigured = false;
    }
    #endregion        
        
    #region Text Configuration
    /// <summary>
    /// Configures the text number on initial state and warn that it has been configured   
    /// </summary>
    private void NumTextConfiguration()
    {
        if(!isNumTextConfigured)
        {
            ConfigText("", textNumberSize, FontStyles.Normal);
            isNumTextConfigured = true;
        }        
    }

    /// <summary>
    /// Configures a text label, text size and style
    /// </summary>
    private void ConfigText(string text, float size, FontStyles style)
    {
        passText.text = text;
        passText.fontSize = size;
        passText.fontStyle = style;
    }
    #endregion
    
    #region Write number on screen
    /// <summary>
    /// Check if num text is configured and writes the input number on screen
    /// </summary>
    public void WriteNum(int num)
    {
        NumTextConfiguration();
        passText.text += num;        
    }
    #endregion
    
    #region Correct and Incorrect screen changes
    /// <summary>
    /// If password is correct, starts correct animation for numbers
    ///     and plays correct sound 
    /// </summary>
    public void SetCorrectAdvice()
    {
        StartCoroutine(TextIntermitent(correctColor, true)); 
        soundsList.PlayAtIndex(0);
    }

    /// <summary>
    /// If password is incorrect, starts incorrect animation for numbers
    ///     and plays incorrect sound 
    /// </summary>
    public void SetIncorrectAdvice()
    {
        StartCoroutine(TextIntermitent(incorrectColor, false)); 
        soundsList.PlayAtIndex(1);
    }

    /// <summary>
    /// Method that animates the text screen when the password is correct and incorrect
    ///     and execute actions for each them 
    /// </summary>
    private IEnumerator TextIntermitent(Color32 color, bool isCorrect)
    {         
        int numIterations = 0;
        string tempText = passText.text;
        passText.color = color;        

        while(numIterations < 2)
        {
            yield return new WaitForSeconds(1f);
            passText.text = "";
            yield return new WaitForSeconds(1f);
            passText.text = tempText;
            numIterations++;
        }

        if(isCorrect)
        {
            passText.text = "OK";
            yield return new WaitForSeconds(1f);
            EjectKeyCard();            
        }
        else
        {
            passText.text = "X";
            yield return new WaitForSeconds(1.5f);
            OnIncorrectPassword();
            Initialize();
        }
        
    }
    #endregion
    
    private void EjectKeyCard()
    {
        Instantiate(keyCardPrefab, keyCardSpawnPosition.position, keyCardSpawnPosition.rotation, transform);
    }
}
