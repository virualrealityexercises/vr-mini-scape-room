using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This script makes it easier to toggle between a new material, and the objects original material.
/// </summary>
public class ChangeMaterial : MonoBehaviour
{
    [SerializeField] 
    Material actual;
    [SerializeField]
    Material other;
    [SerializeField]
    [Tooltip("Specific Button Renderer")]
    private MeshRenderer renderer;    

    public void SetMatDefault()
    {        
        renderer.material = actual;
    }

    public void SetMatOther()
    {
        renderer.material = other;   
    }
}
