using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CongratsMessage : MonoBehaviour
{
    private Vector3 targetMaxPos;
    private Vector3 targetMinPos;
    private bool isAscending;
    private float duration = 1f;
    private float time = 0;
    private Vector3 startPosition;

    // Start is called before the first frame update
    void Start()
    {
        isAscending = true;

        targetMaxPos = new Vector3(transform.position.x, transform.position.y+0.05f, transform.position.z);
        targetMinPos = new Vector3(transform.position.x, transform.position.y-0.05f, transform.position.z);
        startPosition = transform.position;
        StartCoroutine(FloatMessage());
    }

    private IEnumerator FloatMessage(){
        while(true){
            
            if(isAscending){AscendOrDescend(targetMaxPos, false);}
            else { AscendOrDescend(targetMinPos, true); }
            yield return null;
        }       
        
    }

    private void AscendOrDescend(Vector3 targetPosition, bool isAscending){
        if(time < duration){
            MoveFloatingMessage(targetPosition);
            ChangeInitialParametersAndStartPosition(isAscending, targetPosition);
        }
    }

    private void MoveFloatingMessage(Vector3 targetPosition){
        transform.position = Vector3.Lerp(startPosition, targetPosition, time/duration);
        time += Time.deltaTime; 
    }

    private void ChangeInitialParametersAndStartPosition(bool ascending, Vector3 targetPosition){
        if(time >= duration){
            isAscending = ascending;
            transform.position = targetPosition;
            time = 0;
            startPosition = transform.position;
        }
    }
}
