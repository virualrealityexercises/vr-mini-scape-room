using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardReader : MonoBehaviour
{
    public delegate void DoorActivated();
    public static event DoorActivated onKeyCardActivated;

    private bool isStarted;

    private void Start() {
        isStarted = true;
    }

    private void OnTriggerEnter(Collider other) {
        if(other.gameObject.CompareTag("KeyCard")){
            if(isStarted){
                Debug.Log("is started");
                StartCoroutine(WaitForDeactivateDoor());
            }            
        }
    }

    private IEnumerator WaitForDeactivateDoor(){
        isStarted = false;
        yield return new WaitForSeconds(0.5f);
        onKeyCardActivated();
    }

}
