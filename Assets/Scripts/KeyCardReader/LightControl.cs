using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightControl : MonoBehaviour
{
    private ChangeMaterial lightMaterial;
    private PlayQuickSound activateLightSound;

    private void Start(){        
        lightMaterial = GetComponent<ChangeMaterial>();
        activateLightSound = GetComponent<PlayQuickSound>();
        SetDefaultMaterial();
    }

    public void SetDefaultMaterial(){
        lightMaterial.SetMatDefault();
    }

    private void SetOtherMaterial()
    {
        lightMaterial.SetMatOther();
        if(activateLightSound != null){
            PlayClickButtonSound();
        }
    }

    private void PlayClickButtonSound()
    {
        activateLightSound.Play();
    }

    private void OnEnable() {
        CardReader.onKeyCardActivated += SetOtherMaterial;
    }

    private void OnDisable() {
        CardReader.onKeyCardActivated -= SetOtherMaterial;
    }
}
