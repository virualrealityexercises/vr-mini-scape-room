using System.Collections;
using System.Collections.Generic;
using Unity.XRContent.Interaction;
using UnityEngine;

public class DoorLockingBar : MonoBehaviour
{    
    private Rigidbody rigidBody = null;
    private CollisionDetectionMode originalMode = CollisionDetectionMode.Discrete;
    [SerializeField]
    private XRSlider doorSlider;

    // Start is called before the first frame update
    void Awake()
    {
        rigidBody = GetComponent<Rigidbody>();
        DisablePhysics();
        SetDoorSliderState(false);
    }

    private void OnEnable() {
        CardReader.onKeyCardActivated += UnlockDoor;

    }

    private void OnDisable() {
        CardReader.onKeyCardActivated -= UnlockDoor;
    }

    private void UnlockDoor(){
        StartCoroutine(UnlockOverSeconds());
    }

    private IEnumerator UnlockOverSeconds(){        
        Debug.Log("Unlock door");
        yield return new WaitForSeconds(1.0f);
        EnablePhysics();
        SetDoorSliderState(true);
    }

    private void DisablePhysics(){
        originalMode = rigidBody.collisionDetectionMode;
        rigidBody.collisionDetectionMode = CollisionDetectionMode.Discrete;
        rigidBody.useGravity = false;
        rigidBody.isKinematic = true;
    }

    private void EnablePhysics(){
        rigidBody.collisionDetectionMode = originalMode;
        rigidBody.useGravity = true;
        rigidBody.isKinematic = false;
    }

    private void  SetDoorSliderState(bool enabled){
        doorSlider.enabled = enabled;
    }
}
